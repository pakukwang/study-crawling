package crawler;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MainApp {

	private static String URL = "https://www.jobkorea.co.kr/Search/?";

	public static void main(String[] args) throws IOException {
		// TODO 自動生成されたメソッド・スタブ
		String KEY_WORD = "jquery";
		String TYPE = "recruit";
		int PAGE = 1;

		String params = getParameter(KEY_WORD, TYPE, PAGE);

		//1. get doucment
		Document doc = Jsoup.connect(URL + params).get();

		//2. get list
		//System.out.println(doc.toString());

		Elements elements = doc.select(".list-default .post-list-corp");

		//3. get infomataion at list
		int idx = 0;
		for (Element element : elements) {
			System.out.println(++idx + ":" + element.text());
			System.out.println("===============================");
		}
	}


	/**
	 * @param KEY_WORD jquery
	 * @param TYPE recruit
	 * @param PAGE 3
	 * @return
	 */
	public static String getParameter(String KEY_WORD, String TYPE, int PAGE) {
		String params = "stext=" + KEY_WORD + "&tabType=" + TYPE + "&Page_No=" + PAGE;

		return params;
	}
}
